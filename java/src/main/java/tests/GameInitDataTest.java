package tests;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import json.gameinit.GameInitData;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * TEST FOR GameInitData. I know it sucks, but quick and dirty
 * 
 * Also the json in the test files is copied as the server sends it
 */
public class GameInitDataTest {
	public static void main(String[] args) throws Exception {
		BufferedReader in = new BufferedReader(new FileReader(new File("test/game-init-data.json")));
		String json = "";
		while(true) {
			String line = in.readLine();
			if(line == null)
				break;
			json += line.trim();
		}
		json = json.replace(" ", "");
		Gson gson = new GsonBuilder().create();
		GameInitData data = gson.fromJson(json, GameInitData.class);
//		System.out.println(gson.toJson(data));
//		System.out.println(json);
		System.out.println(json.equals(gson.toJson(data)));
		in.close();
	}
}
