package json.gameinit;

import com.google.gson.annotations.SerializedName;

public class Piece {
	public Double length;
	public Integer radius;
	public Double angle;
	@SerializedName("switch")
	public Boolean hasSwitch;
}
