package json.gameinit;

import com.google.gson.GsonBuilder;

public class MessageGameInit {
	public String msgType;
	public GameInitData data;
	public String gameId;
	
	@Override
	public String toString() {
		return (new GsonBuilder()).create().toJson(this);
	}
}
