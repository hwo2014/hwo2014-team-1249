package json.gameinit;

import java.util.List;

public class Track {
	public String id;
	public String name;
	public List<Piece> pieces;
	public List<Lane> lanes;
	public StartingPoint startingPoint;
}
