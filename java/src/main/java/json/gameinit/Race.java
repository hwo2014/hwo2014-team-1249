package json.gameinit;

import java.util.List;

public class Race {
	public Track track;
	public List<Car> cars;
	public RaceSession raceSession;
}
